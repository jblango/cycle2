#IP header
#http://www.networksorcery.com/enp/protocol/ip.htm
#TCP header
#http://www.networksorcery.com/enp/protocol/tcp.htm
#Python script to map activities on TCP, UDP, ICMP from sniffing network packages

import socket          
import sys 
import os                
import errno
import time 
import traceback
from struct import *    # Handle Strings as Binary Data 

# Constants
PROTOCOL_ICMP = 1       # ICMP Protocol
PROTOCOL_IGMP = 2        # IGAP, IGMP RGMP Protocol 
PROTOCOL_TCP  = 6        # TCP Protocol 
PROTOCOL_UDP  = 17        # UDP Protocol

PROTOCOL_MAP = {1:"ICMP", 2:"IGMP", 6:"TCP", 17:"UDP"}
import ctypes

class ICMP(ctypes.Structure):
    _fields_ = [
    ('type',        ctypes.c_ubyte),
    ('code',        ctypes.c_ubyte),
    ('checksum',    ctypes.c_ushort),
    ('unused',      ctypes.c_ushort),
    ('next_hop_mtu',ctypes.c_ushort)
    ]

    def __new__(self, socket_buffer):
        return self.from_buffer_copy(socket_buffer)

    def __init__(self, socket_buffer):
        pass

class UDP(ctypes.Structure):
    _fields_ = [
    ('sourcePort',        ctypes.c_ushort),
    ('destinationPort',   ctypes.c_ushort),
    ('len',               ctypes.c_ushort),
    ('checksum',         ctypes.c_ushort)
    ]
    def __new__(self, socket_buffer):
        return self.from_buffer_copy(socket_buffer)

    def __init__(self, socket_buffer):
        pass

class TCP(ctypes.Structure):
    _fields_ = [
    ('sourcePort',        ctypes.c_ushort),
    ('destinationPort',   ctypes.c_ushort),
    ('sequenceNumber',    ctypes.c_ulong),
    ('ackNumber',         ctypes.c_ulong),
    ('dataOffset',        ctypes.c_ubyte,4),
    ('reserved',          ctypes.c_ubyte,3),
    ('ecn',               ctypes.c_ubyte,3),
    ('controlBits',       ctypes.c_ubyte,6),
    ('window',            ctypes.c_ushort),
    ('checkSum',          ctypes.c_ushort),
    ('urgentPointer',     ctypes.c_ushort)
    ]
    def __new__(self, socket_buffer):
        return self.from_buffer_copy(socket_buffer)

    def __init__(self, socket_buffer):
        pass

class IP(ctypes.Structure):

    _fields_ = [
        ('ihl',           ctypes.c_ubyte, 4),
        ('version',       ctypes.c_ubyte, 4),
        ('tos',           ctypes.c_ubyte),
        ('len',           ctypes.c_ushort),
        ('id',            ctypes.c_ushort),
        ('offset',        ctypes.c_ushort),
        ('ttl',           ctypes.c_ubyte),
        ('protocol_num',  ctypes.c_ubyte),
        ('sum',           ctypes.c_ushort),
        ('src',           ctypes.c_ulong),
        ('dst',           ctypes.c_ulong)
    ]

    def __new__(self, socket_buffer=None):
        return self.from_buffer_copy(socket_buffer)    

    def __init__(self, socket_buffer=None):

        # map protocol constants to their names
        self.protocol_map = {1:"ICMP", 6:"TCP", 17:"UDP"}

        # human readable IP addresses
        self.src_address = socket.inet_ntoa(struct.pack("<L",self.src))
        self.dst_address = socket.inet_ntoa(struct.pack("<L",self.dst))

        # human readable protocol
        try:
            self.protocol = self.protocol_map[self.protocol_num]
        except:
            self.protocol = str(self.protocol_num)


def extractPacker(packet):

    #get the first 20 bytes, the ip header
    ipHeaderPacket = packet[0:20]
     
   
    ipUnpackedHeader = unpack('!BBHHHBBH4s4s' , ipHeaderPacket)
    
                                           
    versionAndLength        = ipUnpackedHeader[0]       
    typeOfService           = ipUnpackedHeader[1]                                              
    packetLength            = ipUnpackedHeader[2]        
    packetIdentification    = ipUnpackedHeader[3]         
    flagFrag                = ipUnpackedHeader[4]        
    Reserved                = (flagFrag >> 15) & 0x01 
    dontFragment            = (flagFrag >> 14) & 0x01 
    moreFragment            = (flagFrag >> 13) & 0x01 
    timeToLive   = ipUnpackedHeader[5]        
    protocol     = ipUnpackedHeader[6]         
    checkSum     = ipUnpackedHeader[7]        
    sourceIPAddress         = ipUnpackedHeader[8]        
    destinationIPAddress    = ipUnpackedHeader[9]            
     
    version      = versionAndLength >> 4            
    length       = versionAndLength & 0x0F           
    ipHdrLength  = length * 4              
       
    sourceAddress      = socket.inet_ntoa(sourceIPAddress);
    destinationAddress = socket.inet_ntoa(destinationIPAddress);
    
    return ([protocol,ipHdrLength,typeOfService, timeToLive, dontFragment],[sourceAddress,destinationAddress])
    
#-------------------------------------------------------------------------------

def processProtocol(packet):
    return(["Filter", "Filter", "Filter"], ["NULL", "OTHER", "NULL", "NULL"])

def processUDP(packet,iphLength,sourceAddress, destinationAddress,typeOfService, timeToLive, dontFragment):
    buf = packet[iphLength:iphLength + ctypes.sizeof(UDP)]
    udpHeader = UDP(buf)
    print("[ UDP ] Source: ", sourceAddress, " = ",udpHeader.sourcePort, "  Dest: ", destinationAddress, " = ",udpHeader.destinationPort)
    synTcp = 999
   
    windowSize = -1
    return([sourceAddress, destinationAddress, udpHeader.destinationPort,"[UDP]"], [synTcp, sourceAddress, typeOfService, timeToLive, dontFragment, windowSize,"[ICMP]"])
    #return(["Filter", "Filter", "Filter"], ["NULL", "UDP", "NULL", "NULL"])

def processICMP(packet, iphLength,sourceAddress,destinationAddress,typeOfService, timeToLive, dontFragment):
    buf = packet[iphLength:iphLength + ctypes.sizeof(ICMP)]
    icmpHeader = ICMP(buf)
    print("[ ICMP ] Source: ", sourceAddress," Dest: ", destinationAddress, " type: ",icmpHeader.type, " code: ", icmpHeader.code)
    #return(["Filter", "Filter", "Filter"], ["NULL", "ICMP", "NULL", "NULL"])
    synTcp = 999
    serverPort = 77777
    windowSize = -1
    return([sourceAddress, destinationAddress, serverPort,"[ICMP]"], [synTcp, sourceAddress, typeOfService, timeToLive, dontFragment, windowSize,"[ICMP]"])

def processTCP(packet,ipHdrLength,sourceAddress,destinationAddress,TOS, timeToLive, dontFragment):
    #print('PROTOCOL_TCP')
    tcpHeaderPacket = packet[ipHdrLength:ipHdrLength+20]   
    tcpHeaderBuffer = unpack('!HHLLBBHHH' , tcpHeaderPacket)
         
    sourcePort             = tcpHeaderBuffer[0]
    destinationPort        = tcpHeaderBuffer[1]
    sequenceNumber         = tcpHeaderBuffer[2]
    acknowledgement        = tcpHeaderBuffer[3]
    dataOffsetandReserve   = tcpHeaderBuffer[4]
    tcpHeaderLength        = (dataOffsetandReserve >> 4) * 4
    flags                  = tcpHeaderBuffer[5]
    finTcp                 = flags & 0x01
    synTcp                    = (flags >> 1) & 0x01
    resetTcp                    = (flags >> 2) & 0x01
    PSH                    = (flags >> 3) & 0x01
    ACK                    = (flags >> 4) & 0x01
    urgent                    = (flags >> 5) & 0x01
    eceTcp                    = (flags >> 6) & 0x01
    cwrTcp                    = (flags >> 7) & 0x01
    windowSize             = tcpHeaderBuffer[6]
    tcpChecksum            = tcpHeaderBuffer[7]
    urgentPointer          = tcpHeaderBuffer[8]
    serverIP   = sourceAddress
    clientIP   = destinationAddress
    serverPort = sourcePort    
    print("[ TCP ] Source: ", sourceAddress, " = ",sourcePort, "  Dest: ", destinationAddress, " = ",destinationPort)
    return([serverIP, clientIP, serverPort,"[TCP]"], [synTcp, serverIP, TOS, timeToLive, dontFragment, windowSize,"[TCP]"])


        
def initSocket(host):
    socket.setdefaulttimeout(5)
    sniffer = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_IP)
    sniffer.bind(( host, 0 ))
    # Include IP headers
    sniffer.setsockopt(socket.IPPROTO_IP, socket.IP_HDRINCL, 1)
    # receive all packages, set promiscuous
    sniffer.ioctl(socket.SIO_RCVALL, socket.RCVALL_ON)
    return sniffer

def processCapture(sniffer,ipObservations,osObservations, maxObservations,printPackage):
    
    while maxObservations > 0:
        try:
            recvBuffer, addr = sniffer.recvfrom(65565)
            if (printPackage == 1):
                print('... Start printing Packet...................................')
                print (recvBuffer)
                print('... End of Packet...')
                print(' ')
                
            v1, v2 = extractPacker(recvBuffer)
            
            protocol = v1[0]
            ipHeader = v1[1]
            typeOfService = v1[2]
            timeToLive = v1[3]
            dontFragment = v1[4]
            sourceIP = v2[0]
            destIP = v2[1]
            #print('-----',protocol, ' ',ipHeader ,'  d=', destIP, '  s=',sourceIP)
            if (protocol == PROTOCOL_ICMP):
                #print('OPROTOCOL_ICMP')
                content, fingerPrint = processICMP(recvBuffer,ipHeader, sourceIP, destIP,typeOfService, timeToLive, dontFragment)
            elif (protocol == PROTOCOL_TCP):
                #print('PROTOCOL_TCP')
                content, fingerPrint = processTCP(recvBuffer,ipHeader, sourceIP, destIP,typeOfService, timeToLive, dontFragment)
            elif (protocol == PROTOCOL_UDP):
                #print('PROTOCOL_UDP')
                content, fingerPrint = processUDP(recvBuffer,ipHeader, sourceIP, destIP,typeOfService, timeToLive, dontFragment)
            else:
                print('PROTOCOL NOT IMPLEMENTED = [', protocol,']')
                content, fingerPrint = processProtocol(recvBuffer)

            if content[0] != "Filter":
                if content[2] > 0:
                    ipObservations.append(content)                            
                    if (fingerPrint[0] == PROTOCOL_ICMP or fingerPrint[0] == PROTOCOL_TCP or fingerPrint[0] == PROTOCOL_UDP):
                        osObservations.append([fingerPrint[1], \
                                                  fingerPrint[2],  \
                                                  fingerPrint[3],  \
                                                  fingerPrint[4],  \
                                                  fingerPrint[5], \
                                                  fingerPrint[6]])
            maxObservations = maxObservations - 1            
            #print('maxObservations =',maxObservations)
            
        except KeyboardInterrupt:
            print('Keyboard Interrupt')
            if os.name == "nt":
                print('turn off promiscous mode....')
                sniffer.ioctl(socket.SIO_RCVALL, socket.RCVALL_OFF)
            return 1
        except:
            tb1 = traceback.format_exc()
            timeOut = "timed out"
            if timeOut in tb1:
                print('...................<time out will continue>........................')
                continue
            else:
                traceback.print_exc(file=sys.stdout)
                print('pass;;;;')
                pass
                    

def generateReport(ipObservations,osObservations,maxObservations):
    print('--------------------generate report-----------')
    #uniqueSrc = set(map(tuple, ipObservations))
    uniqueSrc = ipObservations;
    finalList = list(uniqueSrc)
    finalList.sort()
        
    #uniqueFingerprints = set(map(tuple, osObservations))
    uniqueFingerprints = osObservations
    finalFingerPrintList = list(uniqueFingerprints)
    finalFingerPrintList.sort()
        
        
    # Print out the unique combinations
    print("****************************************************************")
    print ("**********************Unique Packets***********************")
    for packet in finalList:
        print (packet)
          
    print('')      
    print("****************************************************************")
    print ("Unique Fingerprints")
    for osFinger in finalFingerPrintList:
        print (osFinger)
                          
if __name__ == '__main__':
    hostAddress = "127.0.0.1"
    if (len(sys.argv) != 4):
        print(">snifferCapture.py IPADDRESS NumberOfpackets(1..n) displayPacket(0,1)")
        sys.exit("Exiting...")

    hostAddress = sys.argv[1]
    numberOfPackets = int(sys.argv[2])
    printPackage = int(sys.argv[3])
    
    ipObservations = []    
    osObservations = []
    
    try:
        
        sniffer = initSocket(hostAddress)
        #print('processCapture')
        processCapture(sniffer,ipObservations,osObservations, numberOfPackets,printPackage)
        #print('666')
        #generateReport(ipObservations,osObservations,numberOfPackets)
                          
    except:
        print ("Socket Open Failed.........")
        sys.exit()
        
    print ("......................................................................................")
